<?php

require_once('ape.php');
require_once('frog.php');

$hewan = new animal("Sapi");
$hewan2 = new ape("kera sakti");
$hewan3 = new frog("buduk");

echo "name : $hewan->name<br>";
echo "legs : $hewan->legs<br>";
echo "blooded : $hewan->cold_blooded<br><br>";

echo "name : $hewan3->name<br>";
echo "legs : $hewan3->legs<br>";
echo "blooded : $hewan3->cold_blooded<br>";
echo "yell : ".$hewan3->jump()."<br><br>";

echo "name : $hewan2->name<br>";
echo "legs : $hewan2->legs<br>";
echo "blooded : $hewan2->cold_blooded<br>";
echo "yell : ".$hewan2->yell();

?>