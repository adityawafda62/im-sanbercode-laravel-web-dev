<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis(){
        return view('register');
    }

    public function welcome(Request $request){
        $firstName = $request['firstname'];
        $lastName = $request['lastname'];

        return view('welcome', ['firstname' => $firstName, 'lastname' => $lastName]);
    }
    //
}
