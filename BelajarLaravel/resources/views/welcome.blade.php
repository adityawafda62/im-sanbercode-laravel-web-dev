@extends('layout.master')

@section('title')
    Halaman Welcome
@endsection

@section('subtitle')
    Halaman Welcome
@endsection

@section('content')
    <h1>Selamat Datang {{ $firstname }} {{ $lastname }}! </h1>
    <h3>Terimakasih telah bergabung di SanberBook. Social Media kita bersama!</h3>
@endsection