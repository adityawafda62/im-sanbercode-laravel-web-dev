@extends('layout.master')

@section('title')
    Register
@endsection

@section('subtitle')
    Buat Account Baru!
@endsection

@section('content')
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="firstname">First name :</label><br><br>
        <input type="text" name="firstname" id="firstname"><br><br>
        
        <label for="lastname">Last name :</label><br><br>
        <input type="text" name="lastname" id="lastname"><br><br>
        
        <label for="gender">Gender :</label><br><br>
        <input type="radio" name="gender" value="male">Male<br>
        <input type="radio" name="gender" value="female">Female<br>
        <input type="radio" name="gender" value="oth">Other<br><br>
    
        <label for="nation">Nationality :</label><br><br>
        <select name="nation" id="nation">
            <option value="id">Indonesian</option>
            <option value="sg">Singaporean</option>
            <option value="my">Malaysian</option>
        </select><br><br>

        <label for="Language">Languange Spoken :</label><br><br>
        <input type="checkbox" name="language1" value="id">Bahasa Indonesia<br>
        <input type="checkbox" name="language2" value="eng">English<br>
        <input type="checkbox" name="language3" value="oth">Other<br><br>

        <label for="bio">Bio :</label><br><br>
        <textarea name="bio" rows="10" cols="30"></textarea><br><br>

        <button type="submit" name="submit">Sign Up</button>
    </form>
@endsection