@extends('layout.master')
@section('title')
    Halaman Detai Cast
@endsection

@section('subtitle')
    Detail Cast
@endsection

@section('content')

<h1 class="text-primary">{{ $cast->nama }}</h1>
<p>Umur : {{ $cast->umur }} Tahun</p>
<p>{{ $cast->bio }}</p>

@endsection