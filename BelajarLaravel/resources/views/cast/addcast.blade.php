@extends('layout.master')
@section('title')
    Halaman Tambah Cast
@endsection

@section('subtitle')
    Tambah Cast
@endsection

@section('content')
<form action="/cast" method="POST">
  @csrf
  <div class="form-group">
      <label for="nama">Nama Cast</label>
      <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Cast">
      @error('nama')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
  </div>
  <div class="form-group">
      <label for="umur">Umur</label>
      <input type="text" class="form-control" name="umur" id="umur">
      @error('umur')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
  </div>
  <div class="form-group">
      <label for="bio">bio</label><br>
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection