@extends('layout.master')
@section('title')
    Halaman Edit Cast
@endsection

@section('subtitle')
    Edit Cast
@endsection

@section('content')
<form action="/cast/{{ $cast->id }}" method="POST">
  @csrf
  @method('put')
  <div class="form-group">
      <label for="nama">Nama Cast</label>
      <input type="text" class="form-control" value="{{ $cast->nama }}" name="nama" id="nama" placeholder="Masukkan Nama Cast">
      @error('nama')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
  </div>
  <div class="form-group">
      <label for="umur">Umur</label>
      <input type="text" class="form-control" value="{{ $cast->umur }}" name="umur" id="umur">
      @error('umur')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
  </div>
  <div class="form-group">
      <label for="bio">bio</label><br>
      <textarea name="bio" id="bio" cols="30" rows="10">{{ $cast->bio }}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">edit</button>
</form>
@endsection